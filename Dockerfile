FROM registry.cn-shanghai.aliyuncs.com/tmi/openjdk:8-jdk-alpine
WORKDIR /usr/app
COPY target/devtools-web.jar .
COPY run.sh .
CMD ["sh","run.sh"]