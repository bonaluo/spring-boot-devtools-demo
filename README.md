# spring-boot-devtools-demo

### 远程应用

首先你需要通过 `<excludeDevtools>false</excludeDevtools>` 添加 `spring-boot-devtools` 到依赖

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <excludeDevtools>false</excludeDevtools>
    </configuration>
</plugin>
```

其次你需要配置 `spring.devtools.remote.secret` 属性，密钥应该是不容易被猜测的，非常强大的。

注意：这是一种不安全的行为，生产环境不建议使用。

### 运行远程客户端应用

`spring-boot-devtools` 提供了一个主类 `RemoteSpringApplication`，可以连接开启 `spring-boot-devtools` 的远程应用。
你需要启动它并且配置如下环境变量，它的值是远程应用的访问地址，建议使用 `https`。

```properties
nonOptionArgs=http://localhost:8888
```

### 远程更新

远程客户端应用连接上远程应用之后，在本地更改程序内容，然后 `Build Project`。你会发现远程应用被更新了。

在较慢的开发环境中。建议增加 `spring.devtools.restart.poll-interval and spring.devtools.restart.quiet-period`
的值去适配你的环境。增加 `spring.devtools.restart.quiet-period` 的值保证文件更改和编译的时间足够。
