package com.taimi.springbootdevtoolsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author fangyuan.xia
 */
@SpringBootApplication
@EnableScheduling
public class SpringBootDevtoolsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDevtoolsDemoApplication.class, args);
	}

	@Scheduled(fixedRate = 1000)
	public void schedulePrint(){
		final String s = UUID.randomUUID().toString();
		System.out.println(LocalDateTime.now().toString());
	}
}
